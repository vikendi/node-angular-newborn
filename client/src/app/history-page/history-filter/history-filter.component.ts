import {AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, Output, ViewChild} from '@angular/core';
import {Filter} from '../../shared/interfaces';
import {MatherialDatepicker, MatherialService} from '../../shared/classes/material.service';

@Component({
  selector: 'app-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.css']
})
export class HistoryFilterComponent implements AfterViewInit, OnDestroy {

  @Output() Filter = new EventEmitter<Filter>();
  @ViewChild('start') startRef: ElementRef;
  @ViewChild('end') endRef: ElementRef;

  start: MatherialDatepicker;
  end: MatherialDatepicker;
  order: number;

  isValid = true;

  submitFilter() {
    const filter: Filter = {};

    if (this.order) {
      filter.order = this.order;
    }

    if (this.start.date) {
      filter.start = this.start.date;
    }

    if (this.end.date) {
      filter.end = this.end.date;
    }

    this.Filter.emit(filter);
  }

  ngAfterViewInit(): void {
    this.start =  MatherialService.initDatePicker(this.startRef, this.validate.bind(this));
    this.end =  MatherialService.initDatePicker(this.endRef, this.validate.bind(this));
  }
  validate() {
    if (!this.start.date || !this.end) {
      this.isValid = true;
      return;
    }

    this.isValid = this.start.date < this.end.date;
  }

  ngOnDestroy(): void {
    this.start.destroy();
    this.end.destroy();
  }

}
