import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PositionsService} from '../../../shared/services/positions.service';
import {Position} from '../../../shared/interfaces';
import {MatherialInstance, MatherialService} from '../../../shared/classes/material.service';
import {FormGroup, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-positions-form',
  templateUrl: './positions-form.component.html',
  styleUrls: ['./positions-form.component.css']
})
export class PositionsFormComponent implements OnInit, AfterViewInit, OnDestroy {

  // tslint:disable-next-line:no-input-rename
  @Input('categoryId') categoryId: string;
  @ViewChild('modal') modalRef: ElementRef;

  positions: Position[] = [];
  loading = false;
  positionId =  null;
  modal: MatherialInstance;
  form: FormGroup;

  constructor(private positionService: PositionsService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      cost: new FormControl(1, [Validators.required, Validators.min(1)])
    });
    this.loading = true;
    this.positionService.fetch(this.categoryId)
      .subscribe(
        positions => {
          this.loading = false;
          this.positions = positions;
        }
      );
  }

  ngOnDestroy(): void {
    this.modal.destroy();
  }

  ngAfterViewInit(): void {
    this.modal = MatherialService.initModal(this.modalRef);
  }

  onSelectPosition(position: Position) {
    this.positionId = position._id;
    this.form.patchValue({
      name: position.name,
      cost: position.cost,
    });
    this.modal.open();
    MatherialService.updateTextInputs();
  }

  onAddPosition() {
    this.positionId = null;
    this.form.reset({
      name: null,
      cost: 1,
    });
    this.modal.open();
    MatherialService.updateTextInputs();
  }

  onCancel() {
    this.modal.close();
  }
  onSubmit() {
    this.form.disable();
    function completed(self) {
      self.modal.close();
      self.form.reset({name: '', cost: '1'});
      self.form.enable();
    }


    const newPosition: Position = {
      name: this.form.value.name,
      cost: this.form.value.cost,
      category: this.categoryId
    };
    if (this.positionId) {
     newPosition._id = this.positionId;
     this.positionService.update(newPosition)
       .subscribe(
         position => {
           const idx = this.positions.findIndex(p => p._id === position._id);
           this.positions[idx] = position;
           MatherialService.toast('Position was updated');
         },
         _error => MatherialService.toast(_error.error.message),
         () => completed(this)
       );
    } else {
      this.positionService.create(newPosition)
        .subscribe(
          position => {
            MatherialService.toast('Position was created');
            this.positions.push(position);
          },
          _error => MatherialService.toast(_error.error.message),
          () => completed(this)
        );
    }


  }

  onDeletePosition(event: Event, position: Position) {
    event.stopPropagation();
    const decision = window.confirm(`Are you sure you want to delete ${position.name}`);
    if (decision) {
      this.positionService.delete(position)
        .subscribe(
          responce => {
            const idx = this.positions.findIndex(x => x._id === position._id);
            this.positions.splice(idx, 1);
            MatherialService.toast(responce.message);
          },
          _error => MatherialService.toast(_error.error.message)
        );
    }
  }

}
