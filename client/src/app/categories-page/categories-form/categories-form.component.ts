import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CategoriesService} from '../../shared/services/categories.service';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {error} from '@angular/compiler/src/util';
import {MatherialService} from '../../shared/classes/material.service';
import {Category} from '../../shared/interfaces';

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {


  @ViewChild('input') inputRef: ElementRef;

  form: FormGroup;
  image: File;
  imagePreview = '';
  isNew = true;
  category: Category;

  constructor(private route: ActivatedRoute,
              private categoriesService: CategoriesService,
              private router: Router) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required)
    });

    this.form.disable();
    this.route.params
      .pipe(
        switchMap((params: Params) => {
          if (params['id']) {
            this.isNew = false;
            return this.categoriesService.getById(params['id']);
          }
          return of(null);
        })
      )
      .subscribe(
        category => {
          if (category) {
            this.category = category;
            this.form.patchValue({
              name: category.name
            });
            this.imagePreview = category.imageSrc;
            MatherialService.updateTextInputs();
          }
          this.form.enable();
        },
        _error => MatherialService.toast(_error.error.message)
      );
  }

  deleteCategory() {
    const decision = confirm(`Are you sure you want to delete ${this.category.name}?`);

    if (decision) {
      this.categoriesService.delete(this.category._id)
        .subscribe(
          responce => MatherialService.toast(responce.message),
          _error => MatherialService.toast(_error.error.message),
          () => this.router.navigate(['/categories'])
        );
    }
  }

  triggerClick() {
    this.inputRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.image = file;

    const reader = new FileReader();

    reader.onload = () => {
      // @ts-ignore
      this.imagePreview = reader.result;
    };

    reader.readAsDataURL(file);
  }

  onSubmit() {
    let obs$;
    this.form.disable();
    if (this.isNew) {
      obs$ = this.categoriesService.create(this.form.value.name, this.image);
    } else {

      obs$ = this.categoriesService.update(this.category._id, this.form.value.name, this.image);
    }
    obs$.subscribe(
      category => {
        this.category = category;
        MatherialService.toast('Changes was successfully saved');
        this.form.enable();
      },
      _error => {
        MatherialService.toast(_error.error.Message);
        this.form.enable();
      }
    );
  }
}
