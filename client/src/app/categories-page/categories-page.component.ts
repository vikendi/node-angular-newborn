import {Component, NgModule, OnInit} from '@angular/core';
import {CategoriesService} from '../shared/services/categories.service';
import {LoaderComponent} from '../shared/components/loader/loader.component';
import {Category} from '../shared/interfaces';
import {Observable} from "rxjs";

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.css']
})
export class CategoriesPageComponent implements OnInit {
  categories$: Observable<Category[]>;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.categories$ =  this.categoriesService.fetch();
  }

}
