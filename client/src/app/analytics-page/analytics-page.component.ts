import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import AnalyticsService from '../shared/services/analytics.service';
import {AnalyticsPage} from '../shared/interfaces';
import {Subscription} from 'rxjs';
import {Chart} from 'chart.js';


@Component({
  selector: 'app-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.css']
})
export class AnalyticsPageComponent implements AfterViewInit, OnDestroy {
  @ViewChild('gain') gainRef: ElementRef;
  @ViewChild('order') orderRef: ElementRef;

  aSub: Subscription;
  average: number;
  pending = true;

  constructor(private analService: AnalyticsService) {
  }

  ngAfterViewInit(): void {
    const gainConfig: any = {
      label: 'Gain',
      color: 'rgb(255, 99, 132)'
    };

    const orderConfig: any = {
      label: 'Order',
      color: 'rgb(255, 99, 132)'
    };

    this.aSub = this.analService.getAnalyics().subscribe(
      (data: AnalyticsPage) => {
      this.average = data.average;


      gainConfig.labels = data.chart.map(item => item.label);
      gainConfig.data = data.chart.map(item => item.gain);
      orderConfig.labels = data.chart.map(item => item.label);
      orderConfig.data = data.chart.map(item => item.order);

      const gainCts = this.gainRef.nativeElement.getContext('2d');
      const orderCts = this.orderRef.nativeElement.getContext('2d');

      gainCts.canvas.height = '300px';
      orderCts.canvas.height = '300px';
      const chart = new Chart(gainCts, CreateChartConfig(gainConfig));
      const order = new Chart(orderCts, CreateChartConfig(orderConfig));


      this.pending = false;
    });
  }
  ngOnDestroy(): void {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }
}


function CreateChartConfig({labels, data, label, color}) {
  return {
    type: 'line',
    options: {
      responsive: true
    },
    data: {
      labels,
      datasets: [
        {
          label,
          data,
          borderColor: color,
          steppedList: false,
          fill: false
        }
      ]
    }
  };
}
