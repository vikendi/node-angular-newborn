import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MatherialInstance, MatherialService} from '../shared/classes/material.service';
import {OrderService} from './order.service';
import {Order, OrderPosition} from '../shared/interfaces';
import {OrdersService} from '../shared/services/orders.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css'],
  providers: [OrderService]
})

export class OrderPageComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('modal') modalRef: ElementRef;
  isRoot: boolean;
  pending = false;
  oSub: Subscription;
  modal: MatherialInstance;

  constructor(private router: Router,
              private order: OrderService,
              private ordersService: OrdersService) {
  }

  ngOnInit() {
    this.isRoot = this.router.url === '/order';
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isRoot = this.router.url === '/order';
      }
    });
  }

  ngAfterViewInit(): void {
   this.modal =  MatherialService.initModal(this.modalRef);
  }

  ngOnDestroy(): void {
    this.modal.destroy();
    if (this.oSub) {
      this.oSub.unsubscribe();
    }
  }

  removePosition(orderPosition: OrderPosition) {
    this.order.remove(orderPosition);
  }

  open() {
    this.modal.open();
  }
  cancel() {
    this.modal.close();
  }
  submit() {
  this.pending = true;
    const order: Order = {
      list: this.order.list.map(p => {
        delete p._id;
        return p;
      })
    };

    this.oSub = this.ordersService.create(order).subscribe(
      newOrder => {
        MatherialService.toast(`Order №${newOrder.order} was added`);
        this.order.clear();
      },
      _error => MatherialService.toast(_error.error.message),
      () => {
        this.modal.close();
        this.pending = false;
      }
    );
  }

}
