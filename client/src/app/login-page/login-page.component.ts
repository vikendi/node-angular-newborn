import { MatherialService } from './../shared/classes/material.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  aSyb: Subscription;

  constructor(private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute) {
  }


  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });

    this.route.queryParams.subscribe((params: Params) => {
      if (params['registered']) {
        MatherialService.toast('Now you can enter using your data');
        // Now you can enter using your data
      } else if (params['accessDenied']) {
        MatherialService.toast('Please login first');
      } else if (params['sessionFailted']) {
        MatherialService.toast('Please, log in again');
      }
    });
  }

  ngOnDestroy(): void {
    if (this.aSyb) {
      this.aSyb.unsubscribe();
    }
  }

  onSubmit() {
    this.form.disable();

    this.aSyb = this.auth.login(this.form.value).subscribe(
      () => this.router.navigate(['/overview']),
      (error) => {
        MatherialService.toast(error.error.message);
        this.form.enable();
      }
    );
  }

}
