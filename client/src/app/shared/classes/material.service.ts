import {ElementRef} from '@angular/core';

declare var M;

export interface MatherialInstance {
  open?(): void;
  close?(): void;
  destroy?(): void;
}

export interface MatherialDatepicker extends MatherialInstance {
  date?: Date;
}

export class MatherialService {
    static toast(message: string) {
        M.toast({html: message} );
    }

    static  initializeFloatingButton(ref: ElementRef) {
      M.FloatingActionButton.init(ref.nativeElement);
    }

    static updateTextInputs() {
      M.updateTextFields();
    }
    static initModal(ref: ElementRef): MatherialInstance  {
      return M.Modal.init(ref.nativeElement);
    }

    static initToolTip(ref: ElementRef): MatherialInstance {
      return M.Tooltip.init(ref.nativeElement);
    }

    static initDatePicker(ref: ElementRef, onClose: () => void) {
      return M.Datepicker.init(ref.nativeElement, {
        format: 'dd.mm.yyyy',
        showClearBtn: true,
        onClose
      });
    }
    static initTapTarget(ref: ElementRef): MatherialInstance {
      return M.TapTarget.init(ref.nativeElement);
    }
}
