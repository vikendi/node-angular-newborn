import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message, Position} from '../interfaces';

@Injectable({
  providedIn: 'root'
})

export class PositionsService {
  constructor(private http: HttpClient) {
  }

  fetch(categoryId: string): Observable<Position[]> {
    return this.http.get<Position[]>(`/api/position/${categoryId}`);
  }

  create(postion: Position): Observable<Position> {
    return this.http.post<Position>('/api/position', postion);
  }

  update(postion: Position): Observable<Position> {
    return this.http.patch<Position>(`/api/position/${postion._id}`, postion);
  }
  delete(postion: Position): Observable<Message> {
    return this.http.delete<Message>(`/api/position/${postion._id}`);
  }
}
