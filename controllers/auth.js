const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const errorHandler = require('../utils/errorHandler');

const User = require('../models/User');


module.exports.login = async function (req, res) {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        //users exists
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password);
        if (passwordResult) {
            //token generation
            const token = jwt.sign({
                email: candidate.email,
                userId: candidate._id
            }, keys.jwt, {expiresIn: 60 * 60});

            res.status(200).json({
                token: `Bearer ${token}`
            })
        } else {
            res.status(401).json({
                //passwords didn't match
                message: "Wrong password. Please, try again"
            })
        }
    } else {
       //no user
        res.status(404).json({
            message: "user not found"
        })
    }
};

module.exports.register = async function (req, res) {
    //email, password
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        //If user exists - send error
        res.status(409).json({
            message: 'Email already exists'
        });
    } else {
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;
        const user = new User({
            email: req.body.email,
            password: bcrypt.hashSync(password, salt)
        });
        try {
            await user.save();
            res.status(201).json(user)
        } catch (e) {
            errorHandler(res, error);
        }

    }
};