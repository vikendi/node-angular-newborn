const moment = require('moment');
const Orders = require('../models/Orders');
const errorHandler = require('../utils/errorHandler');

module.exports.overview = async function (req, res) {

    try {
        const allOrders = await Orders.find({user: req.user.id}).sort({date: 1});
        const ordersMap = getOrdersMap(allOrders);
        const yesterdayOrders = ordersMap[moment().add(-1, 'd').format('DD.MM.YYYY')] || [];


        //Orders yesterday
        const yesterdayOrdersNumber = yesterdayOrders.length;
        //Order's number
        const totalOrdersNumber = allOrders.length;
        //Total day's
        const daysNumber = Object.keys(ordersMap).length;
        //Orders per day
        const ordersPerDay = (totalOrdersNumber / daysNumber).toFixed(0);
        //Percentage for orders quantity
        //((ordersYesterday\ordersPerDay) - 1) * 100
        const ordersPercent = (((yesterdayOrdersNumber/ordersPerDay) - 1) * 100).toFixed(2);
        //total Gain
        const totalGain = calculatePrice(allOrders);
        // Gain per day
        const gainPerDay = totalGain / daysNumber;
        //Gain for yesterday
        const yesterdayGain = calculatePrice(yesterdayOrders);
        // Percentage of Gain
        const gainPercent = (((yesterdayGain/gainPerDay) - 1) * 100).toFixed(2);
        //Gain Compare
        const  compareGain = (yesterdayGain-gainPerDay).toFixed(2);
        //Orders number Compare
        const compareNumber = (yesterdayOrdersNumber - ordersPerDay).toFixed(2);

        res.status(200).json({
            gain: {
                percent: Math.abs(+gainPercent),
                compare: Math.abs(+compareGain),
                yesterday: +yesterdayGain,
                isHigher: +gainPercent> 0,
            },
            orders: {
                percent: Math.abs(+ordersPercent),
                compare: Math.abs(+compareNumber),
                yesterday: +yesterdayOrdersNumber,
                isHigher: +ordersPercent> 0,
            }
        })
    } catch (e) {
        errorHandler(res,e);
    }

};
module.exports.analytics = async function (req, res) {

    try {
        const allOrders = await Orders.find({user: req.user.id}).sort({date: 1});
        const ordersMap = getOrdersMap(allOrders);

        const average = +(calculatePrice(allOrders) / Object.keys(ordersMap).length).toFixed(2);

        const chart = Object.keys(ordersMap).map(label => {
            const gain = calculatePrice(ordersMap[label]);
            const order = ordersMap[label].length;
            return {label, order, gain}
        });
        // label = "01.02.19"

        res.status(200).json({average, chart});

    } catch (e) {
        errorHandler(res, e);
    }
};

function getOrdersMap(orders = []) {
    const daysOrders = {};

    orders.forEach((order)=> {
       const date = moment(order.date).format('DD.MM.YYYY');

       if (date === moment().format('DD.MM.YYYY')) {
          return
       }

       if(!daysOrders[date]) {
           daysOrders[date] = []
       }

       daysOrders[date].push(order);
    });
    return daysOrders
}

function calculatePrice(orders = []) {
    return orders.reduce((total, order) => {
        const orderPrice = order.list.reduce((orderTotal, item) => {
            return orderTotal += item.cost * item.quantity
        }, 0)
        return total += orderPrice
    }, 0)
}
